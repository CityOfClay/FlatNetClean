package flatnetclean;

/**
 * @author MultiTool
 */
public class FlatNetClean {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    MainGui mgoo = new MainGui();
    mgoo.Init();
  }
}

/*
quehacer:
push this
change closest-finder on curve to start from flat closest (from normal) - done
make everything linear, and make it work.
. must init training states to linear ramp.
make nodein attract all of its motes to its plane - done?

change to this:
in main draw me, pick a mote number MoteNum = (gencnt mod num_motes)
pass MoteNum to first layer?  each layer must pass motenum through axon when it fires? 

in flatnet:
closest mote is hit.
closest mote moves closer to hit location.
closest mote desire vector calculated and passed to upstream links.

next time upstreamer fires, before it fires, it grabs downstream correctors and applies them to stale closest mote. 
then it fires and picks a new closest mote.  
so desire rests in downstream links and is applied upstream just before upstream mote fires and is updated.

*/