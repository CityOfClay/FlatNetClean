package flatnetclean;

import static flatnetclean.Things.BoxSpacing;
import static flatnetclean.Things.ColumHeight;
import java.util.ArrayList;

/**
 *
 * @author MultiTool
 */
/* ****************************************************** */
public class Layers implements Things.Drawable {
  public ArrayList<Network> Network_List;
  public double xorg, yorg;
  Network net_first, net_last;
  double InFire0, InFire1, GoalFire;
  NodeIn Input0, Input1;

  public int GenCnt;
  public Layers() {
    GenCnt = 0;
    Network_List = new ArrayList<Network>();
//    xorg = 50; yorg = 50;
    xorg = 10;
    yorg = 25;
  }
  /* ****************************************************** */
  public void Make_Layers(int num_layers, int Layer_Size) {
    int last_layer = num_layers - 1;

    this.Input0 = new NodeIn();
    this.Input1 = new NodeIn();

//    NodeIn FactoryIn = new NodeIn();
    NodeOut FactoryOut = new NodeOut();

    int lcnt = 0;
    {// Input layer
      Network net = new Network();
//      net.Make_Layer(FactoryIn, Layer_Size);
      net.Make_Layer(Input0, Input1);
      net.xorg = (lcnt / ColumHeight) * (BoxSpacing * (Layer_Size + 0.5));
      net.yorg = (lcnt % ColumHeight) * BoxSpacing;
      Network_List.add(net);

      // Remember the first layer so we can feed it input.
      this.net_first = net;

      lcnt++;
    }
    while (lcnt < last_layer) {// Middle layers
      Network net = new Network();
      net.Make_Layer(Layer_Size);
      net.xorg = (lcnt / ColumHeight) * (BoxSpacing * (Layer_Size + 0.5));
      net.yorg = (lcnt % ColumHeight) * BoxSpacing;
      Network_List.add(net);
      lcnt++;
    }
    {// Output layer
      Network net = new Network();
      net.Make_Layer(FactoryOut, Layer_Size);
      net.xorg = (lcnt / ColumHeight) * (BoxSpacing * (Layer_Size + 0.5));
      net.yorg = (lcnt % ColumHeight) * BoxSpacing;
      Network_List.add(net);

      // Remember the last layer so we can set the desired output heights.
      this.net_last = net;

      lcnt++;
    }

    // Connect layers
    Network net_prev = this.Network_List.get(0);
    for (lcnt = 1; lcnt < num_layers; lcnt++) {
      Network net = this.Network_List.get(lcnt);
      if (true) {
        net.Connect_All_To_All(net_prev);// all to all
      } else {
        net.Connect_Weave(net_prev);// 2D array connect
      }
      net_prev = net;
    }

    // Make Planes and Motes.
    for (lcnt = 0; lcnt < num_layers; lcnt++) {
      Network net = this.Network_List.get(lcnt);
      net.Create_Guts();
    }

    // Initialize guts as random or otherwise
    boolean RandInit = true;
    this.net_first.Init_Unique();
    if (RandInit) {
      this.net_first.Init_Random_Unique();
    } else {
      this.net_first.Init_Unique();
    }
    for (lcnt = 1; lcnt < num_layers; lcnt++) {
      Network net = this.Network_List.get(lcnt);
      net_prev = net;
      if (RandInit) {
        net.Init_Random_Unique();
      } else {
        net.Init_Unique();
      }
    }

  }
  /* ****************************************************** */
  public void RunCycle() {
    this.GenCnt++;
    {
      double Amp = 2.0;//, HalfAmp = Amp / 2.0;
      int MoteNum = this.GenCnt % 4;
      int InBit0 = (MoteNum & 1);
      int InBit1 = ((MoteNum >> 1) & 1);
      // this.InFire1 = InBit1 * Amp - HalfAmp;
      this.InFire0 = (((double) InBit0) - 0.5) * Amp;
      this.InFire1 = (((double) InBit1) - 0.5) * Amp;
      int OutBit = InBit0 ^ InBit1;
//      this.GoalFire = OutBit * Amp - HalfAmp;
      this.GoalFire = (((double) OutBit) - 0.5) * Amp;
    }
    // Generate and feed inputs to first layer.
    this.Input0.Assign_Fire(this.InFire0);
    this.Input1.Assign_Fire(this.InFire1);

    System.out.println("in1:" + this.InFire0 + ", in1:" + this.InFire1 + ", GoalFire:" + this.GoalFire);

    this.Cascade();

    // feed this.GoalFire to the currently firing mote of just the output layer
    this.net_last.Assign_Mote_Height(this.GoalFire);

//    this.Distribute_Outfire();
//    this.Pass_Back_Correctors();   
//    this.Gather_And_Apply_Correctors();
    double AvgFitError = this.Get_Avg_FitError();
    System.out.println("GenCnt:" + this.GenCnt + ", Average Fit Error:" + AvgFitError);
//    System.out.println("MoteNum:" + NodeIn.MoteNum + ", Average Fit Error:" + AvgFitError);
//    this.net_last.Init_And();
//    this.net_last.Init_Xor();
    /* collect and fire to nowfire (all)
     * for all {
     *   calc desire 
     *   pass back desire (to links)
     * }
     * for all {
     *   collect desire from links
     *   apply desire to my prevfire
     *   push nowfire to links, set prevfire to nowfire.
     * }
     * 
     * each node only has a nowfire.  prev fire is stored in links.
     * 
     */
//    System.out.println("Layers RunCycle EXIT");
  }
  /* ****************************************************** */
  public double Get_Avg_FitError() {
//    System.out.println("Layers Get_Avg_FitError");
    int num_layers = Network_List.size();
    int start_layer = 0;// first few layers should be getting *more* nonlinear
    double Sum = 0.0;
    for (int lcnt = start_layer; lcnt < num_layers; lcnt++) {
      Network net = this.Network_List.get(lcnt);
      Sum += net.Get_Avg_FitError();
    }
    double FitError = Sum / (double) (num_layers - start_layer);
    if (Double.isNaN(FitError)) {
      System.out.println();// breakpoint
    }
//    System.out.println("Layers Get_Avg_FitError EXIT");
    return FitError;
  }
  /* ****************************************************** */
  @Override public void Draw_Me(Things.DrawingContext dc) {
//    System.out.println("Layers Draw_Me");
    Things.DrawingContext mydc = new Things.DrawingContext();
    mydc.gr = dc.gr;
    mydc.transform.Accumulate(dc.transform, this.xorg, this.yorg, 1.0, 1.0);

    int Num_Layers = Network_List.size();
    for (int lcnt = 0; lcnt < Num_Layers; lcnt++) {
      Network net = this.Network_List.get(lcnt);
      net.Draw_Me(mydc);
    }
//    System.out.println("Layers Draw_Me EXIT");
  }
  /* ****************************************************** */
  public void Cascade() {
    int Num_Layers = Network_List.size();
    for (int LayerCnt = 0; LayerCnt < Num_Layers; LayerCnt++) {
      Network net = this.Network_List.get(LayerCnt);
      net.RunCycle();
    }
  }
  /*
   @Override public void Collect_And_Fire() {// Causal
   //    System.out.println("Layers Collect_And_Fire");
   int Num_Layers = Network_List.size();
   for (int LayerCnt = 0; LayerCnt < Num_Layers; LayerCnt++) {
   Network net = this.Network_List.get(LayerCnt);
   net.Collect_And_Fire();// read all inlinks for fires, compute my own fire
   }
   }
   @Override public void Distribute_Outfire() {// Causal
   //    System.out.println("Layers Distribute_Outfire");
   int Num_Layers = Network_List.size();
   for (int LayerCnt = 0; LayerCnt < Num_Layers; LayerCnt++) {
   Network net = this.Network_List.get(LayerCnt);
   net.Distribute_Outfire();
   }
   }
   @Override public void Pass_Back_Correctors() {// Causal
   //    System.out.println("Layers Pass_Back_Correctors");
   int Num_Layers = Network_List.size();
   if (true) {
   int last_layer = Num_Layers - 1;
   for (int lcnt = last_layer; lcnt >= 0; lcnt--) {// apply backward
   Network net = this.Network_List.get(lcnt);
   net.Pass_Back_Correctors();
   }
   } else {
   for (int lcnt = 0; lcnt < Num_Layers; lcnt++) {// apply forward
   Network net = this.Network_List.get(lcnt);
   net.Pass_Back_Correctors();
   }
   }
   }
   @Override public void Gather_And_Apply_Correctors() {// Causal
   //    System.out.println("Layers Gather_And_Apply_Correctors");
   int Num_Layers = Network_List.size();
   int last_layer = Num_Layers - 1;
   for (int lcnt = last_layer; lcnt >= 0; lcnt--) {
   //      System.out.println("Layers Gather_And_Apply_Correctors lcnt:" + lcnt);
   Network net = this.Network_List.get(lcnt);
   net.Gather_And_Apply_Correctors();
   }
   //    System.out.println("Layers Gather_And_Apply_Correctors EXIT");
   }
   */
  /* ****************************************************** */
//  public void Assign_Ring_Mote_Height(double Height) { // never use this
//    int Num_Layers = Network_List.size();
//    for (int lcnt = 0; lcnt < Num_Layers; lcnt++) {
//      Network net = this.Network_List.get(lcnt);
//      net.Assign_Ring_Mote_Height(Height);
//    }
//  }
}
