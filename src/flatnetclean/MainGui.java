package flatnetclean;

import flatnetclean.Things.DrawingContext;
import java.awt.AWTEvent;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * @author MultiTool
 */
public class MainGui {
  JFrame frame;
  DrawingPanel drawpanel;
  Layers layers = null;
  Network network = null;
  boolean KeysEnabled = true;
  boolean SlowMotion = false;
  boolean Syncing = false;
  /* ********************************************************************************* */
  public MainGui() {
    Things.ColumHeight = 16;
    Things.ColumHeight = 8;
    {
      int Layer_Size = 2;
      layers = new Layers();
//    layers.Make_Layers(2, Layer_Size);
//    layers.Make_Layers(3, Layer_Size);// too tight to work
//    layers.Make_Layers(4, Layer_Size);
//    layers.Make_Layers(6, Layer_Size);
//    layers.Make_Layers(7, Layer_Size);
      layers.Make_Layers(8, Layer_Size);
//    layers.Make_Layers(12, Layer_Size);
//    layers.Make_Layers(16, Layer_Size);
//    layers.Make_Layers(32, Layer_Size);
//    layers.Make_Layers(64, Layer_Size);
//    layers.Make_Layers(128, Layer_Size);
//    layers.Make_Layers(256, Layer_Size);
    }
    if (false) {
      int num_nodes = 1;
//      num_nodes = 2;
//      num_nodes = 3;
//      num_nodes = 4;
//      num_nodes = 5;
//      num_nodes = 6;
//      num_nodes = 7;
      num_nodes = 8;
//      num_nodes = 12;
//      num_nodes = 14;
//      num_nodes = 16;
//      Things.ColumHeight = 8;// does not matter
      this.network = new Network();
      this.network.Make_Layer(num_nodes);
      this.network.Connect_Self_Loop();
      this.network.Create_Guts();
    } else {
      int num_dims = 1;
      num_dims = 3;
//      num_dims = 4;
      this.network = new Network();
      this.network.Create_Hypercube(num_dims);
      this.network.Create_Guts();
    }
  }
  /* ********************************************************************************* */
  public void Init() {
    this.frame = new JFrame();
    this.frame.setTitle("Fibers");
//    this.frame.setSize(700, 400);
//    this.frame.setSize(1000, 900);
    this.frame.setSize(1250, 900);
    this.frame.addWindowListener(new WindowAdapter() {
      @Override public void windowClosing(WindowEvent e) {
        System.exit(0);
      }
    });
    Container contentPane = this.frame.getContentPane();
    this.drawpanel = new DrawingPanel();
    contentPane.add(this.drawpanel);
    this.drawpanel.BigApp = this;
    frame.setVisible(true);

    {// https://tips4java.wordpress.com/2009/08/30/global-event-listeners/
      long EventMask = AWTEvent.MOUSE_MOTION_EVENT_MASK + AWTEvent.MOUSE_EVENT_MASK + AWTEvent.KEY_EVENT_MASK;
      EventMask = AWTEvent.KEY_EVENT_MASK;
      Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {//final DrawingPanel dp = this.drawpanel;
        KeyEvent keprev = null;
        @Override public void eventDispatched(AWTEvent Event) {
          KeyEvent ke = (KeyEvent) Event;
//          if (keprev != ke) {
          HandleKeys(ke);
          keprev = ke;
//          }
        }
      }, EventMask);
    }
  }
  /* ********************************************************************************* */
  public void HandleKeys(KeyEvent ke) {
    DrawingPanel dpnl = this.drawpanel;
    System.out.println("keyPressed:" + ke.getKeyCode() + ":" + ke.getExtendedKeyCode() + ":" + ke.getModifiers() + ":" + ke.getKeyChar() + ":" + ke.getModifiersEx());
    char ch = Character.toLowerCase(ke.getKeyChar());
    int keycode = ke.getKeyCode();
    int mod = ke.getModifiers();
    String JsonTxt = "";
    if (KeysEnabled) {
      KeysEnabled = false;// to do: think of a better debouncer
      boolean CtrlPress = ((mod & KeyEvent.CTRL_MASK) != 0);
      if (CtrlPress) {
        if ((keycode == KeyEvent.VK_C) && CtrlPress) {
        } else if ((keycode == KeyEvent.VK_S)) {
        } else if ((keycode == KeyEvent.VK_X)) {
        } else if ((keycode == KeyEvent.VK_T)) {
        } else if ((keycode == KeyEvent.VK_E)) {
        } else if ((keycode == KeyEvent.VK_S)) {// ctrl S means save
        } else if ((keycode == KeyEvent.VK_O)) {// ctrl O means open
        } else if ((keycode == KeyEvent.VK_Q)) {// ctrl Q means quit
          System.exit(0);
        } else if (keycode == KeyEvent.VK_ESCAPE) {
        }
      } else {
        if (keycode == KeyEvent.VK_DELETE) {
        } else if (keycode == KeyEvent.VK_S) {
          this.SlowMotion = true;//!this.SlowMotion;
        } else if ((keycode == KeyEvent.VK_Q)) {// ctrl Q means quit
          this.SlowMotion = false;//!this.SlowMotion;
        } else if (keycode == KeyEvent.VK_R) {
          this.Syncing = false;
          this.network.Explode();
        } else if (keycode == KeyEvent.VK_I) {
//          this.network.SyncPlaneFire();
          this.Syncing = true;
        }
      }
      System.out.println("*********************************** Key pressed!");
      KeysEnabled = true;
    }
    System.out.println(ke.getID());
  }
  /* ****************************************************** */
  public void RunCycleRecurrent() {
    network.RunCycle();
    // feed this.GoalFire to the currently firing mote of just the output layer
//    network.Assign_Mote_Height(this.GoalFire);
    double AvgFitError = network.Get_Avg_FitError();
    System.out.println("GenCnt:" + this.network.GenCnt + ", Average Fit Error:" + AvgFitError);
  }
  /* ********************************************************************************* */
  public static class DrawingPanel extends JPanel implements MouseMotionListener, MouseListener, MouseWheelListener, ComponentListener, KeyListener {
    MainGui BigApp;
    int ScreenMouseX = 0, ScreenMouseY = 0;
    double MouseOffsetX = 0, MouseOffsetY = 0;

    /* ********************************************************************************* */
    public DrawingPanel() {
      this.Init();
    }
    /* ********************************************************************************* */
    public final void Init() {
      this.addMouseListener(this);
      this.addMouseMotionListener(this);
      this.addMouseWheelListener(this);
      this.addKeyListener(this);
    }
    /* ********************************************************************************* */
    public void Draw_Me(Graphics2D g2d) {
//      System.out.println("MainGui Draw_Me");
      // quehacer: create paramblob for drawing context, use it in things
      g2d.setBackground(Color.yellow);
      g2d.setBackground(Color.black);
      g2d.clearRect(0, 0, this.getWidth(), this.getHeight());
      Things.TransForm tr = new Things.TransForm();

      int wdt, hgt;

      wdt = this.getWidth();
      hgt = this.getHeight();

      Rectangle2D rect = new Rectangle2D.Float();
      rect.setRect(0, 0, wdt, hgt);

      Stroke oldStroke = g2d.getStroke();
      BasicStroke bs = new BasicStroke(5f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
      g2d.setStroke(bs);
      g2d.setColor(Color.green);
      g2d.draw(rect);// green rectangle confidence check for clipping
      g2d.setStroke(oldStroke);
      DrawingContext dc = new DrawingContext();
      dc.gr = g2d;
      dc.transform = tr;
      if (Globals.Recurrent) {
        Things.DrawingContext mydc = new Things.DrawingContext();
        mydc.gr = dc.gr;
        mydc.transform.Accumulate(dc.transform, 0.0, 100.0, 3.0, 3.0);
        BigApp.network.Draw_Me(mydc);
        if (this.BigApp.Syncing) {
          System.out.println("*************************************** Syncing!");
          BigApp.network.SyncPlaneFire();
          BigApp.network.Distribute_Outfire();// pass fire to outlinks
          this.BigApp.Syncing = false;
        }
        BigApp.RunCycleRecurrent();
      } else {
        Things.DrawingContext mydc = new Things.DrawingContext();
        mydc.gr = dc.gr;
//        mydc.transform.Accumulate(dc.transform, 4, 1, 5.0, 5.0);
//        mydc.transform.Accumulate(dc.transform, 0, 0, 4.0, 4.0);
//        mydc.transform.Accumulate(dc.transform, 0, 0, 3.0, 3.0);
        mydc.transform.Accumulate(dc.transform, 0, 0, 2.0, 2.0);
//        mydc.transform.Accumulate(dc.transform, 0, 0, 1.0, 1.0);
        BigApp.layers.Draw_Me(mydc);
        BigApp.layers.RunCycle();
      }
      if (this.BigApp.SlowMotion) {
        try {
          Thread.sleep(2000);
//        Thread.sleep(100);
        } catch (Exception ex) {
        }
      }
//      System.out.println("MainGui Draw_Me EXIT");
    }
    /* ********************************************************************************* */
    @Override public void paintComponent(Graphics g) {
      super.paintComponent(g);
      Graphics2D g2d = (Graphics2D) g;
      Draw_Me(g2d);// redrawing everything is overkill for every little change or move. quehacer: optimize this
      this.repaint();
//      try {
//        Thread.sleep(100);
//      } catch (Exception ex) {
//      }
    }
    /* ********************************************************************************* */
    @Override public void mouseDragged(MouseEvent me) {
    }
    @Override public void mouseMoved(MouseEvent me) {
      this.ScreenMouseX = me.getX();
      this.ScreenMouseY = me.getY();
    }
    /* ********************************************************************************* */
    @Override public void mouseClicked(MouseEvent me) {
    }
    @Override public void mousePressed(MouseEvent me) {
      this.repaint();
    }
    @Override public void mouseReleased(MouseEvent me) {
      this.repaint();
    }
    @Override public void mouseEntered(MouseEvent me) {
    }
    @Override public void mouseExited(MouseEvent me) {
    }
    /* ********************************************************************************* */
    @Override public void mouseWheelMoved(MouseWheelEvent mwe) {
      double XCtr, YCtr, Rescale;
      XCtr = mwe.getX();
      YCtr = mwe.getY();
      double finerotation = mwe.getPreciseWheelRotation();
      this.repaint();
    }
    /* ********************************************************************************* */
    @Override public void componentResized(ComponentEvent ce) {
    }
    @Override public void componentMoved(ComponentEvent ce) {
    }
    @Override public void componentShown(ComponentEvent ce) {
    }
    @Override public void componentHidden(ComponentEvent ce) {
    }
    /* ********************************************************************************* */
    @Override public void keyTyped(KeyEvent ke) {
      boolean nop = true;// breakpoint
    }
    @Override public void keyPressed(KeyEvent ke) {
      boolean nop = true;// breakpoint
      int key = ke.getKeyCode();
      if (key == KeyEvent.VK_LEFT) {
//        dx = -1;
      } else if (key == KeyEvent.VK_RIGHT) {
//        dx = 1;
      } else if (key == KeyEvent.VK_UP) {
//        dy = -1;
      } else if (key == KeyEvent.VK_DOWN) {
//        dy = 1;
      }
    }
    @Override public void keyReleased(KeyEvent ke) {
      boolean nop = true;// breakpoint
    }
  }

}
