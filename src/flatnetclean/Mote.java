package flatnetclean;

import static flatnetclean.Things.lrate;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;

/**
 *
 * @author MultiTool
 */
/* ****************************************************** */
public class Mote extends PointNd implements Things.Drawable {// Control Point
  int Num_Upstreamers = 0;
  int Num_Downstreamers = 0;
  public double OutFire;
  PointNd screenloc = new PointNd(2);// temporary but often-reused
  PointNd LineEnd = new PointNd(2);// temporary but often-reused
  PointNd MoteHeightBar = new PointNd(2);// temporary but often-reused
  PointNd Attraction;
  double radius, diameter;
  public NodeBox Parent;
  /* ****************************************************** */
  public Mote(NodeBox NewParent, int num_dims) {
    super(num_dims);
    Attraction = new PointNd(num_dims);
    this.Parent = NewParent;
    for (int cnt = 0; cnt < num_dims; cnt++) {
      this.loc[cnt] = 0.0;
    }
    Num_Upstreamers = 0;
    radius = 4.0;
    diameter = radius * 2.0;
    OutFire = 0;
  }
  /* ****************************************************** */
  @Override public double Get_Height() {
    return this.loc[this.ninputs];
  }
  /* ****************************************************** */
  @Override public void Set_Height(double Height) {
    this.loc[this.ninputs] = Height;
  }
  public double Get_Mapped_Outfire() {
    return this.Parent.Mote_ActFun(this.Get_Height());
  }
  @Override public void Draw_Me(Things.DrawingContext dc) {
    Things.TransForm tr = dc.transform;
    Graphics2D gr = dc.gr;
    tr.To_Screen(this.loc[0], this.loc[1], screenloc);

    double height = this.loc[ninputs];
    height = this.Parent.Mote_ActFun(height);// parent can represent as raw or curved
    Color pnt_color = this.Height_Color(height);

    gr.setColor(pnt_color); // need to add code to xmap color here
    gr.fillRect((int) (screenloc.loc[0] - radius), (int) (screenloc.loc[1] - radius), (int) diameter, (int) diameter);

    double Height = this.Get_Height();
    tr.To_Screen(this.loc[0], this.loc[1] + Height, MoteHeightBar);

    if (false) {
      gr.setColor(Color.black); // draw mote height
      gr.drawLine((int) (screenloc.loc[0]), (int) (screenloc.loc[1]), (int) (MoteHeightBar.loc[0]), (int) (MoteHeightBar.loc[1]));
      gr.setColor(Color.white); // draw mote height
      gr.drawLine((int) (screenloc.loc[0] + 1), (int) (screenloc.loc[1]), (int) (MoteHeightBar.loc[0] + 1), (int) (MoteHeightBar.loc[1]));
    }

    if (false) {
      gr.setColor(pnt_color); // draw mote height rectangle
      gr.fillRect((int) (screenloc.loc[0]), (int) (screenloc.loc[1]), (int) (1), (int) (MoteHeightBar.loc[1] - screenloc.loc[1]));
      gr.setColor(Color.black); // draw mote height rectangle outline
      gr.drawRect((int) (screenloc.loc[0]), (int) (screenloc.loc[1]), (int) (1), (int) (MoteHeightBar.loc[1] - screenloc.loc[1]));
    }
    gr.setColor(Color.black);
    gr.drawRect((int) (screenloc.loc[0] - radius), (int) (screenloc.loc[1] - radius), (int) diameter, (int) diameter);

    if (true) {
      // Draw attraction vector.
      double Exaggeration = 20.0;
      double x1 = this.loc[0] + this.Attraction.loc[0] * Exaggeration;
      double y1 = this.loc[1] + this.Attraction.loc[1] * Exaggeration;
      tr.To_Screen(x1, y1, LineEnd);
      gr.setColor(Color.green);
      gr.drawLine((int) (screenloc.loc[0]), (int) (screenloc.loc[1]), (int) (LineEnd.loc[0]), (int) (LineEnd.loc[1]));
      gr.setColor(Color.orange);
      gr.drawLine((int) (screenloc.loc[0] + 1), (int) (screenloc.loc[1] + 0), (int) (LineEnd.loc[0] + 1), (int) (LineEnd.loc[1] + 0));
    }
    /*
     * {
     * if (true) { double factor = 100.0; gr.setColor(Color.magenta); * draw
     * the movement line * gr.drawLine( (int) (xorg + Bounds.Rad(0) *
     * pnt.loc[0]), (int) (yorg + Bounds.Rad(1) * pnt.loc[1]), (int) (xorg +
     * Bounds.Rad(0) * (pnt.loc[0] + pnt.delta[0] * factor)), (int) (yorg +
     * Bounds.Rad(1) * (pnt.loc[1] + pnt.delta[1] * factor))); } }
     */
  }
  public void Draw_Highlight(Things.DrawingContext dc, Color EdgeColor) {
    Things.TransForm tr = dc.transform;
    Graphics2D gr = dc.gr;
    tr.To_Screen(this.loc[0], this.loc[1], screenloc);

//    Stroke StrokePrev = gr.getStroke();
//    gr.setStroke(new BasicStroke(2));
//    gr.setColor(EdgeColor);
    gr.drawRect((int) (screenloc.loc[0] - radius), (int) (screenloc.loc[1] - radius), (int) diameter, (int) diameter);
//    gr.setStroke(StrokePrev);
  }
  public Color Height_Color(double height) {
    float pntband = (float) ((height + 1.0) / 2.0);// map to range 0.0 - 1.0
    float red = pntband, blue = (float) 1.0 - pntband;
    blue = blue < 0 ? 0 : (blue > 1 ? 1 : blue);// clip color range
    red = red < 0 ? 0 : (red > 1 ? 1 : red);// clip color range
    Color pnt_color = new Color(red, 0.0f, blue);
    return pnt_color;
  }
  public void Apply_Corrector(double CorrectorParam) {
    double Height = this.Get_Height();
    if (true) {
      CorrectorParam *= Globals.sigmoid_deriv(Height);// ??? do we need this
    }
    this.Set_Height(Height + CorrectorParam);
//    this.loc[ninputs] += CorrectorParam;
  }
}
