package flatnetclean;

import static flatnetclean.Things.lrate;
import static flatnetclean.Things.ndims_init;
import java.util.ArrayList;

/**
 *
 * @author Multitool
 */
//public class Mote_Cloud {
//  
//}
/* ****************************************************** */
public class Mote_Cloud extends ArrayList<Mote> {
  public NodeBox Parent;
  public int MoteNum = 0, LastMoteNum;
  public Mote_Cloud(NodeBox NewParent, int newsize) {
    double RandAmp = 0.01;
    this.Parent = NewParent;
    PointNd avg = new PointNd(ndims_init);
    for (int cnt = 0; cnt < newsize; cnt++) {
      Mote pnt = new Mote(this.Parent, ndims_init);
      pnt.Randomize(-1.0 * RandAmp, 1.0 * RandAmp);
      this.add(pnt);
    }
    avg.Multiply(newsize);// experiment to move all the points to the centroid
  }
  /* ****************************************************** */
  public void Get_Average(PointNd ret_avg) {
    ret_avg.Clear();
    for (PointNd pnt : this) {
      ret_avg.Add(pnt);
    }
    ret_avg.Multiply(1.0 / (double) this.size());
  }
  /* ****************************************************** */
  public void CheckNAN() {
    for (PointNd pnt : this) {
      pnt.CheckNAN();
    }
  }
  /* ****************************************************** */
  public int Find_Closest(PointNd HitPnt) {
    int NumMotes = this.size();
    Mote mote;
    int ClosestMoteNum = -1;
    double Distance, MinDist = Double.POSITIVE_INFINITY;
    for (int MoteCnt = 0; MoteCnt < NumMotes; MoteCnt++) {
      mote = this.get(MoteCnt);
      Distance = HitPnt.Get_Distance(mote);
      if (MinDist > Distance) {
        MinDist = Distance;
        ClosestMoteNum = MoteCnt;
      }
    }
    return ClosestMoteNum;
  }
  /* ****************************************************** */
  public Mote Get_Next_Mote() {// Increment in a cycle and get next mote.
    this.MoteNum = (this.MoteNum < this.LastMoteNum) ? this.MoteNum + 1 : 0;// Get oldest mote.
    Mote Mote_Now = this.get(this.MoteNum);
    return Mote_Now;
  }
  /* ****************************************************** */
  @Override public boolean add(Mote e) {
    this.LastMoteNum = this.size();
    return super.add(e);
  }
  /* ****************************************************** */
  public void Fatten(Mote mote) {
    Mote other;// Fatten pushes motes apart vertically to fatten the signal.
    int Num_Motes = this.size();
    double HalfAmp = 1.0, Local_Lrate = lrate * lrate * lrate * lrate;
    double Gap, Goal, Factor, Offset, Radius, FractAlong, FractAlongSqrt;
    double Height, OtherHeight, Min = Double.MAX_VALUE, Max = -Double.MAX_VALUE, Mid;
    for (int cnt = 0; cnt < Num_Motes; cnt++) {
      other = this.get(cnt);
      OtherHeight = other.Get_Height();
      if (Min > OtherHeight) {
        Min = OtherHeight;
      }
      if (Max < OtherHeight) {
        Max = OtherHeight;
      }
    }
    Mid = (Min + Max) / 2.0;
    Height = mote.Get_Height();
    if (true) {
      Height -= Mid;// center it
      Radius = Max - Mid;
      FractAlong = (Height / Radius);// stretching experiment - flee the ramp
//      FractAlongSqrt = Math.sqrt(FractAlong);// stretching experiment - flee the ramp
      Gap = HalfAmp - (Radius / HalfAmp);// space between range and limit
      Goal = Radius + (Gap * Local_Lrate);
      Factor = Goal / Radius;
      Height *= Factor;
      Offset = Mid * (1.0 - Local_Lrate);
      Height += Offset;// put it back
    }
    if (false) {
      Height = Math.pow(Math.abs(Height), 0.99) * Math.signum(Height);// flee 0.0
    }
    mote.Set_Height(Height);
    // mote.Attraction.Add(Delta, mote.ninputs);
  }
  /* ****************************************************** */
  public void Flee(Mote mote) {// Flee other motes who have different heights.
    Mote other;// Flee pushes motes apart to fatten the signal.
    int Num_Motes = this.size();
    double HeightDiff, Distance, Force;
    double Radius = 0.6;// works for squares
    Radius = Math.sqrt(2.0) / 2.0;
//    Radius = 0.5;
//    Radius = 0.3;
//    Radius = 1.0;
    if (false) {
      Fatten(mote);
    } else {
//      Fatten(mote);
      PointNd Delta = new PointNd(mote.ndims);
      double LocalLRate = Math.pow(lrate, 1);
      for (int cnt = 0; cnt < Num_Motes; cnt++) {
        Delta.Copy_From(mote);
        Delta.Set_Height(0.0);
        other = this.get(cnt);
        Delta.Subtract(other, mote.ninputs);// direction from other to me is now Delta
        Distance = mote.Get_Distance(other, mote.ninputs);
        if (Distance > 0.0) {
          HeightDiff = mote.Get_Height() - other.Get_Height();
          HeightDiff = Math.abs(HeightDiff);// ?
          HeightDiff *= 0.5;// difference range would be 0 to 2, so divide by 2
//          HeightDiff -= 0.02;// attempt at attraction for similar heights, this forces a little consolidation and doesn't seem to hurt.
//          Force = HeightDiff / (Distance * Distance * Distance);
//          Delta.Multiply(Force * LocalLRate, mote.ninputs);
//          Delta.CheckNAN();
//          mote.Attraction.Add(Delta, mote.ninputs);
          if (Distance < Radius) {
            Force = (Radius - Distance) / Radius;
//            Force = Math.pow(Force, mote.ninputs);// experiment, power of distance
//            Force = Math.pow(Force, 6.0);// experiment, power of distance
            Force *= HeightDiff * 100;
            Force /= (double) Num_Motes;
            Delta.Multiply(Force * LocalLRate, Delta.ninputs);
            Delta.CheckNAN();
            mote.Attraction.Add(Delta, mote.ninputs);
          }
        }
      }
      if (false) {// really bad idea here
        double Height = mote.Get_Height();
        Height = Math.pow(Math.abs(Height), 0.99) * Math.signum(Height);// flee 0.0
        mote.Set_Height(Height);
      }
      if (false) {// experiment to randomize when closer to 0.0
        double Height = mote.Get_Height();
        double RandAmp = Globals.sigmoid_deriv(Height);
        RandAmp *= 0.01;
//        mote.Jitter(-RandAmp, RandAmp);
        mote.Attraction.Jitter(-RandAmp, RandAmp, mote.ninputs);
      }
    }
  }
}
