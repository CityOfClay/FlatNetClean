package flatnetclean;

import static flatnetclean.NodeBox.ExplodeCnt;
import static flatnetclean.Things.BoxSpacing;
import java.util.ArrayList;

/**
 *
 * @author MultiTool
 */
/* ****************************************************** */
public class Network implements Things.Drawable, Things.Causal {
  public ArrayList<NodeBox> Node_List;
  public double xorg, yorg;
  public int GenCnt;
  /* ****************************************************** */
  public Network() {
    Node_List = new ArrayList<NodeBox>();
    this.GenCnt = 0;
  }
  /* ****************************************************** */
  @Override public void Draw_Me(Things.DrawingContext dc) {
//    System.out.println("Network Draw_Me");
    Things.DrawingContext mydc = new Things.DrawingContext();
    mydc.gr = dc.gr;
    mydc.transform.Accumulate(dc.transform, this.xorg, this.yorg, 1.0, 1.0);
    int Num_Nodes = this.Node_List.size();
    NodeBox node;
    for (int NCnt = 0; NCnt < Num_Nodes; NCnt++) {
      node = Node_List.get(NCnt);
      node.Draw_Me(mydc);
    }
//    System.out.println("Network Draw_Me EXIT");
  }
  public void Make_Layer(int num_nodes) {
//    this.Make_Layer(new NodeBox(), num_nodes);
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      NodeBox nb = new NodeBox();
      nb.xorg = (ncnt + 1.0) * BoxSpacing;
      Node_List.add(nb);
    }
  }
  /* ****************************************************** */
  public void Make_Layer(NodeBox... members) {
    int num_nodes = members.length;
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      NodeBox nb = members[ncnt];
      nb.xorg = (ncnt + 1.0) * BoxSpacing;
      Node_List.add(nb);
    }
  }
  /* ****************************************************** */
  public void Make_Layer(NodeBox factory, int num_nodes) {
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      NodeBox nb = factory.Clone_Me();
      nb.xorg = (ncnt + 1.0) * BoxSpacing;
      Node_List.add(nb);
    }
  }
  /* ****************************************************** */
  public void Create_Guts() {// Make Planes and Motes.
    int num_nodes = this.Node_List.size();
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      NodeBox nb = this.Node_List.get(ncnt);
      nb.Create_Guts();
    }
  }
  public void SyncPlaneFire() {
    int num_my_nodes = this.Node_List.size();
    for (int NodeCnt = 0; NodeCnt < num_my_nodes; NodeCnt++) {
      NodeBox nb = this.Node_List.get(NodeCnt);
      nb.SyncPlaneFire();
    }
  }
  public void Init_Random_Unique() {
    int num_nodes = this.Node_List.size();
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      NodeBox nb = this.Node_List.get(ncnt);
      nb.Init_Random_Unique(nb.Motes);
    }
  }
  public void Init_Unique() {
    int num_nodes = this.Node_List.size();
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      NodeBox nb = this.Node_List.get(ncnt);
      nb.Init_Unique(nb.Motes);
    }
  }
  /* ****************************************************** */
  public void Connect_Self_Loop() {// Connect to self in a loop
    NodeBox NodePrev, NodeNow;

    int NumNodes = this.Node_List.size();
    int LastNodeNum = NumNodes - 1;

    NodePrev = this.Node_List.get(LastNodeNum);
    for (int cnt = 0; cnt < NumNodes; cnt++) {// X connections
      NodeNow = this.Node_List.get(cnt);
      NodeNow.ConnectIn(NodePrev);
//      NodePrev.ConnectIn(NodeNow);
      NodePrev = NodeNow;
    }
    NodePrev = this.Node_List.get(LastNodeNum);
    for (int cnt = 0; cnt < NumNodes; cnt++) {// Y connections
      NodeNow = this.Node_List.get(cnt);
      NodePrev.ConnectIn(NodeNow);
      NodePrev = NodeNow;
    }
  }
  /* ****************************************************** */
  public void Create_Hypercube(int numdims) {//, double xorg, double yorg) {
    int arraysize = 1 << numdims;
//    Create_Nodes(arraysize, xorg, yorg);
    this.Make_Layer(arraysize);
    int xloc = 0;
    int yloc = 0;
    int numcols = 8;
    int bitmask;

    for (int cnt = 0; cnt < arraysize; cnt++) {
      NodeBox node = this.Node_List.get(cnt);
//      if (false) {
//        xloc = (int) (nodewdt * (cnt % numcols));
//        yloc = (int) (nodewdt * (cnt / numcols));
//        node.xorg = (int) xorg + xloc;
//        node.yorg = (int) yorg + yloc;
//      }
      bitmask = 0;
      for (int bcnt = 0; bcnt < numdims; bcnt++) {
        bitmask = 1 << bcnt;
        int otherdex = cnt ^ bitmask; // xor flips one bit
        NodeBox othernode = this.Node_List.get(otherdex);
        node.ConnectIn(othernode);
      }
    }
    if (false) {
      // cross connect opposite corner
      bitmask = ~0;// invert a zero to make all 1's
      bitmask = bitmask << numdims;// shift left to make numdims 0's
      bitmask = ~bitmask;// invert again, to have numdims 1's
      for (int cnt = 0; cnt < arraysize; cnt++) {
        NodeBox node = this.Node_List.get(cnt);
        for (int bcnt = 0; bcnt < numdims; bcnt++) {
          int otherdex = cnt ^ bitmask; // xor flips all bits
          NodeBox othernode = this.Node_List.get(otherdex);
          node.ConnectIn(othernode);
        }
      }
    }
  }
  /* ****************************************************** */
  public void Connect_Weave(Network other) {
    // Connect two meshes in a weave
    int num_my_nodes = this.Node_List.size();
    int num_other_nodes = other.Node_List.size();
    if (num_my_nodes != num_other_nodes) {
      System.exit(1);// boom!  networks have to be the same size
    }

    NodeBox us, ds;
    int prevcnt = num_my_nodes - 1;// loop 

    for (int cnt = 0; cnt < num_my_nodes; cnt++) {
      ds = this.Node_List.get(cnt);

      us = other.Node_List.get(prevcnt);
      ds.ConnectIn(us);

      us = other.Node_List.get(cnt);
      ds.ConnectIn(us);

      prevcnt = cnt;
    }
  }
  public void Connect_All_To_All(Network other) {
    // Connect all-to-all between two meshes
    int num_my_nodes = this.Node_List.size();
    int num_other_nodes = other.Node_List.size();

    for (int ncnt0 = 0; ncnt0 < num_other_nodes; ncnt0++) {
      NodeBox us = other.Node_List.get(ncnt0);
      for (int ncnt1 = 0; ncnt1 < num_my_nodes; ncnt1++) {
        NodeBox ds = this.Node_List.get(ncnt1);
        ds.ConnectIn(us);
      }
    }
    for (int ncnt1 = 0; ncnt1 < num_my_nodes; ncnt1++) {
      NodeBox ds = this.Node_List.get(ncnt1);
      //ds.Init_States(4);// must rethink this
      //ds.Init_States(1 << ds.Num_Us);// wrong wrong wrong
    }
  }
  @Override public void Collect_And_Fire() {// Causal
    int num_my_nodes = this.Node_List.size();
    for (int NodeCnt = 0; NodeCnt < num_my_nodes; NodeCnt++) {
      NodeBox nb = this.Node_List.get(NodeCnt);
      nb.Collect_And_Fire();
    }
  }
  @Override public void Distribute_Outfire() {// Causal
    int num_my_nodes = this.Node_List.size();
    for (int NodeCnt = 0; NodeCnt < num_my_nodes; NodeCnt++) {
      NodeBox nb = this.Node_List.get(NodeCnt);
      nb.Distribute_Outfire();
    }
  }
  @Override public void Pass_Back_Correctors() {// Causal
    int num_my_nodes = this.Node_List.size();
    for (int ncnt1 = 0; ncnt1 < num_my_nodes; ncnt1++) {
      NodeBox nb = this.Node_List.get(ncnt1);
      nb.Pass_Back_Correctors();
    }
  }
  @Override public void Gather_And_Apply_Correctors() {// Causal
    int num_my_nodes = this.Node_List.size();
    for (int ncnt1 = 0; ncnt1 < num_my_nodes; ncnt1++) {
      NodeBox nb = this.Node_List.get(ncnt1);
      nb.Gather_And_Apply_Correctors();
    }
  }
  public double Get_Avg_FitError() {
    int num_nodes = this.Node_List.size();
    double Sum = 0.0;
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      NodeBox nb = this.Node_List.get(ncnt);
      Sum += nb.FitError;
    }
    return Sum / (double) num_nodes;
  }
  /* ****************************************************** */
  public void RunCycle() {
    this.Collect_And_Fire();// read all inlinks for fires, compute my own fire
    if (Globals.Recurrent) {
      NodeBox PortNode = this.Node_List.get(0);

      int beat;
//      beat = 2;
//      beat = 3;
//      beat = 4;
//      beat = 5;
      beat = 6;
//      beat = 7;
//      beat = 8;
      double Value = this.GenCnt % beat == 0 ? 1.0 : -1.0;

      System.out.println("Value:" + Value + ", MoteFire:" + PortNode.MoteFire + ", PlaneFire:" + PortNode.PlaneFire);

      if (true) {// input and training
        PortNode.Assign_Mote_Height(Value);
        PortNode.MoteFire = Value;
        PortNode.PlaneFire = Value;
      }

      if (false) {
        if (this.GenCnt % 2000 == 0) {
          this.SyncPlaneFire();
        }
      }
    }
    this.Distribute_Outfire();// pass fire to outlinks
    this.Pass_Back_Correctors();// pass to inlinks
    this.Gather_And_Apply_Correctors();// gather from inlinks and apply to motes, etc.
    this.GenCnt++;
  }
  /* ****************************************************** */
  public void Assign_Mote_Height(double Height) {
    int num_nodes = this.Node_List.size();
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      NodeBox nb = this.Node_List.get(ncnt);
      nb.Assign_Mote_Height(Height);
    }
  }
  /* ****************************************************** */
  public void Explode() {// in case of emergency break everything
    int num_nodes = this.Node_List.size();
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      NodeBox nb = this.Node_List.get(ncnt);
      nb.Explode();
    }
  }
}
