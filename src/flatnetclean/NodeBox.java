package flatnetclean;

import static flatnetclean.Things.lrate;
import java.awt.Color;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 *
 * @author MultiTool
 */
/* ****************************************************** */
public class NodeBox implements Things.Drawable, Things.Causal {
  public Mote_Cloud Motes = null;
  public int Ring_Max = 14;//8;//  //, Ring_Last = Ring_Max - 1, RingMoteNumNow = 0;
  public Mote Mote_Now = null, Mote_Prev = null;
  public int Num_Us, Num_Ds;// snox do we need this?
  public ArrayList<Axon> US, DS;
  public Roto_Plane planeform = null;
  public double xorg, yorg, xscale, yscale;
  public double FitError, FitErrorFlywheel = 0.0;
  double MoteFire, PlaneFire;
  PointNd PlaneHitPoint = null;
//  public static boolean TestIncrement = true;
  /* ****************************************************** */
  public static class Axon {// link to other Nodes
    public NodeBox US, DS;
    public double Corrector;//, FireVal;
    public double MoteFire, PlaneFire = 0.0;
    public Axon() {
    }
    public Axon(NodeBox US0, NodeBox DS0) {
      US = US0;
      DS = DS0;
      US0.DS.add(this);
      DS0.US.add(this);
      US0.Num_Ds++;// snox do we need this?
      DS0.Num_Us++;
    }
  }
  /* ****************************************************** */
  public NodeBox() {
    xscale = yscale = 12.0;
    Num_Us = Num_Ds = 0;
    this.US = new ArrayList<Axon>();
    this.DS = new ArrayList<Axon>();
  }
  /* ****************************************************** */
  void Create_Guts() {// Make Plane and Motes.
    int NInputs = this.US.size();// Dimensions are based on number of inputs.
    int NumDims = NInputs + 1;// Plus one dimension for output height.
    this.planeform = new Roto_Plane(NumDims);
    this.PlaneHitPoint = new PointNd(NInputs);
    this.Init_Mote_Cloud(NumDims);
  }
  public void SyncPlaneFire() {
    this.PlaneFire = this.MoteFire;
  }
  /* ****************************************************** */
  public void Init_Mote_Cloud(int NumDims) {
    double RandAmp = 0.01;
    int NumInputs = NumDims - 1;
    RandAmp = 0.1;
    RandAmp = 0.5;
    RandAmp = 1.0 / NumInputs;
    this.Motes = new Mote_Cloud(this, 0);
    Mote mote;
    for (int cnt = 0; cnt < this.Ring_Max; cnt++) {
      mote = new Mote(this, NumDims);
      this.Motes.add(mote);
      mote.Randomize(-RandAmp, RandAmp);// quehacer: remove this and leave randomizing to layers.make_layers.  
    }
    this.Mote_Prev = this.Mote_Now = this.Motes.Get_Next_Mote();
  }
  /* ****************************************************** */
  @Override public void Draw_Me(Things.DrawingContext dc) {
    Things.DrawingContext mydc = new Things.DrawingContext();
    mydc.gr = dc.gr;
    mydc.transform.Accumulate(dc.transform, this.xorg, this.yorg, this.xscale, this.yscale);

    PointNd boxmin = new PointNd(2);
    PointNd boxmax = new PointNd(2);
    double xmin = -1.0, ymin = -1.0, xmax = 1.0, ymax = 1.0;
    mydc.transform.To_Screen(xmin, ymin, boxmin);
    mydc.transform.To_Screen(xmax, ymax, boxmax);

    dc.gr.setColor(Color.green);
    dc.gr.drawRect((int) (boxmin.loc[0]), (int) (boxmin.loc[1]), (int) (boxmax.loc[0] - boxmin.loc[0]), (int) (boxmax.loc[1] - boxmin.loc[1]));

    this.planeform.Draw_Me(mydc);

    if (true) {
      PointNd Zero = new PointNd(2);
      mydc.transform.To_Screen(0, 0, Zero);
      PointNd ErrPnt = new PointNd(2);
      mydc.transform.To_Screen(xmin, this.FitError, ErrPnt);
      dc.gr.setColor(Color.black);
      dc.gr.fillRect((int) (boxmax.loc[0]), (int) (Zero.loc[1]), (int) (3), (int) (ErrPnt.loc[1] - Zero.loc[1]));
      dc.gr.setColor(Color.white);
      dc.gr.drawRect((int) (boxmax.loc[0]), (int) (Zero.loc[1]), (int) (3), (int) (ErrPnt.loc[1] - Zero.loc[1]));
    }

    for (Mote mote : this.Motes) {
      mote.Draw_Me(mydc);
    }
    if (false) {
      this.Mote_Now.Draw_Highlight(mydc, Color.green);// Highlight the active mote
    }

    if (true) {// plot actual infire as a + 
      int ptsz = 8;
      int halfptsz = ptsz / 2;
      mydc.gr.setColor(Color.green);
      PointNd TicCtr = new PointNd(2);
      mydc.transform.To_Screen(this.PlaneHitPoint.loc[0], this.PlaneHitPoint.loc[1], TicCtr);
      int xmid = (int) (TicCtr.loc[0]);
      int ymid = (int) (TicCtr.loc[1]);

      int x0 = xmid - halfptsz;
      int y0 = ymid - halfptsz;
      int x1 = x0 + ptsz;
      int y1 = y0 + ptsz;
      mydc.gr.drawLine(xmid, y0, xmid, y1);// vertical line
      mydc.gr.drawLine(x0, ymid, x1, ymid);// horizontal line
    }
  }
  /* ****************************************************** */
  public void Init_Random_Unique(Mote_Cloud Motes) {// hacky
    double amp = 2.0;
    int Num_Motes = Motes.size();
    Mote cpnt;
    double level = 0.0, minjump = 0.1, range = 0.1;
    for (int pcnt = 0; pcnt < Num_Motes; pcnt++) {// random sorted
      cpnt = Motes.get(pcnt);
      level += minjump + (Logic.wheel.nextDouble() * range);
      cpnt.loc[cpnt.ninputs] = (((double) level) - 0.5) * amp;
    }
    if (true) {// random shuffled
      double temp;
      int randex;
      Mote cpnt0, cpnt1;
      for (int pcnt = 0; pcnt < Num_Motes; pcnt++) {
        randex = Logic.wheel.nextInt(Num_Motes);
        cpnt0 = Motes.get(pcnt);
        cpnt1 = Motes.get(randex);
        temp = cpnt0.loc[cpnt0.ninputs];
        cpnt0.loc[cpnt0.ninputs] = cpnt1.loc[cpnt1.ninputs];
        cpnt1.loc[cpnt1.ninputs] = temp;
      }
    }
  }
  public void Init_Unique(Mote_Cloud Motes) {
    double amp = 2.0;
    int num_pnts = Motes.size();
    int last_pnt = num_pnts - 1;
    Mote cpnt;
    for (int pcnt = 0; pcnt < num_pnts; pcnt++) {
      cpnt = Motes.get(pcnt);
      double val = ((double) pcnt) / (double) last_pnt;
      cpnt.Set_Height((((double) val) - 0.5) * amp);
      System.out.println(cpnt.loc[cpnt.ninputs]);
    }
  }
  public void Init_Same(Mote_Cloud Motes, double Value) {// nonsense value, only for testing
    int num_pnts = Motes.size();
    for (int pcnt = 0; pcnt < num_pnts; pcnt++) {
      Mote cpnt = Motes.get(pcnt);
      cpnt.loc[cpnt.ninputs] = Value;
    }
  }
  public void Line_Up_Motes(Mote_Cloud Motes) {
    double amp = 2.0, halfamp = amp / 2.0;
    int Num_Motes = Motes.size();
    double value, Num_Spaces = Num_Motes - 1;
    for (int MCnt = 0; MCnt < Num_Motes; MCnt++) {
      Mote mote = Motes.get(MCnt);
      value = ((double) MCnt) / Num_Spaces;
      value = (value * amp) - halfamp;
      mote.Fill(value);
    }
  }
  public void Flatten_Motes(Mote_Cloud Motes) {// put all motes flat on curve
    double value = 0.0;
    int Num_Motes = Motes.size();
    for (int MCnt = 0; MCnt < Num_Motes; MCnt++) {
      Mote mote = Motes.get(MCnt);
      value = this.planeform.Get_Plane_Height(mote);
//      value = Globals.General_ActFun(value);
      mote.Set_Height(value);
      mote.Jitter(-0.0001, 0.0001);
    }
  }
  public void ConnectIn(NodeBox upstreamer) {
    this.ConnectInAxon(upstreamer);
    this.Num_Us++;
    upstreamer.Num_Ds++;
  }
  public void ConnectInAxon(NodeBox upstreamer) {
    Axon axon = new Axon(upstreamer, this);
//      upstreamer.DS.add(axon); this.US.add(axon);
  }
  /* ****************************************************** */
  @Override public void Collect_And_Fire() {// Causal
    // Collect from all InLinks, output to MoteFire and PlaneFire
    int InCnt, NumIns = this.US.size();// == this.planeform.ninputs
    Axon InLink;// technically a dendrite
    double MoteFireUS, PlaneFireUS;
    PointNd HitPoint = new PointNd(NumIns);
    this.PlaneHitPoint = new PointNd(NumIns);
    for (InCnt = 0; InCnt < NumIns; InCnt++) {
      InLink = this.US.get(InCnt);
      MoteFireUS = InLink.MoteFire;
      HitPoint.loc[InCnt] = MoteFireUS;// Just moves my mote to the XY location of my input fires
      PlaneFireUS = InLink.PlaneFire;
      this.PlaneHitPoint.loc[InCnt] = PlaneFireUS;
    }

    this.PlaneFire = this.planeform.Get_Plane_Height(this.PlaneHitPoint);
    this.PlaneFire = Globals.General_ActFun(this.PlaneFire);// Do this here?

    {
      int ClosestNum = -1;
      Mote ClosestMote = null;
      this.Mote_Prev = this.Mote_Now;
      ClosestNum = this.Motes.Find_Closest(HitPoint);
      try {
        ClosestMote = this.Motes.get(ClosestNum);
      } catch (Exception ex) {
        boolean nop = true;// breakpoint
      }
      this.Mote_Now = this.Motes.Get_Next_Mote();
      this.Mote_Now.Copy_From(HitPoint);// Assign XY loc from axon inputs.
      this.Mote_Now.Set_Height(ClosestMote.Get_Height());// Assign Z loc from height of closest mote.
      this.MoteFire = this.Mote_Now.Get_Height();
    }

//    System.out.println("OutFire:" + this.Mote_Now.Get_Height() + ", Ring:" + this.Ring_Mote_Now.Get_Height());
    if (true) {// disabled for testing
      this.Ping_Plane();
    }
//    this.planeform.Jitter(-0.0001, 0.0001);
  }
  @Override public void Distribute_Outfire() {// Causal
    int OutCnt, NumOuts = this.DS.size();
    Axon OutLink;
    for (OutCnt = 0; OutCnt < NumOuts; OutCnt++) {
      OutLink = this.DS.get(OutCnt);
      OutLink.MoteFire = this.MoteFire;
      OutLink.PlaneFire = this.PlaneFire;
    }
  }
  public void Self_Correct(Mote mote) {
    if (true) {// self-move can be disabled for testing
      mote.Apply_Corrector(mote.Attraction.Get_Height() * lrate);// vfactor
    }
  }
  public void Pass_Back_Correctors(Mote_Cloud Motes, Mote mote) {
    int InCnt, NumIns = this.US.size();// == this.planeform.ninputs
    Roto_Plane plane = this.planeform;
    Axon InLink;// really a dendrite from my point of view
    plane.Find_Closest_On_Curve(mote);// First generate the corrector 
    Motes.Flee(mote);// And generate another corrector to space out the motes
    for (InCnt = 0; InCnt < NumIns; InCnt++) {
      InLink = this.US.get(InCnt);
      InLink.Corrector = mote.Attraction.loc[InCnt];// only apply lrate at the last minute
    }
  }
  @Override public void Pass_Back_Correctors() {// Causal
    Mote Hot_Mote = null;
    Mote_Cloud Cloud = null;
    Hot_Mote = this.Mote_Now;
    Cloud = this.Motes;
    Pass_Back_Correctors(Cloud, Hot_Mote);
    this.Self_Correct(Hot_Mote);
  }
  @Override public void Gather_And_Apply_Correctors() {// Causal
    int DSCnt, NumDS = this.DS.size();
    Axon OutLink;
    double Corrector = 0.0;
    for (DSCnt = 0; DSCnt < NumDS; DSCnt++) {
      OutLink = this.DS.get(DSCnt);
      Corrector += OutLink.Corrector;
    }

    this.Mote_Prev.Apply_Corrector(Corrector * lrate);// only apply lrate at the last minute
    this.FitError = this.GetFitError(this.Motes);// metrics

    this.FitErrorFlywheel = this.FitErrorFlywheel * 0.99 + this.FitError * 0.01;
    if (this.FitErrorFlywheel > 0.1) {
//      this.Explode();
    }
  }
  /* ****************************************************** */
  public void Assign_Mote_Height(double Height) {
    this.Mote_Now.Set_Height(Height);
  }
  /* ****************************************************** */
  public double Mote_ActFun(double rawheight) {// virtual
    return rawheight;
  }
  /* ****************************************************** */
  public void Ping_Plane() {
    Mote mote;
    mote = this.Mote_Now;
    this.planeform.Ping(mote);
  }
  public double GetFitError(Mote_Cloud Motes) {
    int NumMotes = Motes.size();
    Mote mote;
    double Height, HgtDiff, Sum = 0.0, Avg, AvgRoot = 0.0;
    for (int pcnt = 0; pcnt < NumMotes; pcnt++) {
      mote = Motes.get(pcnt);
      Height = planeform.Get_Plane_Height(mote);
      if (Double.isNaN(Height)) {
        System.out.println();// breakpoint
      }
      Height = Globals.General_ActFun(Height);
      HgtDiff = Height - mote.Get_Height();// curve space
      Sum += HgtDiff * HgtDiff;
    }
    Avg = Sum / (double) NumMotes;
    AvgRoot = Math.sqrt(Avg);
    if (Double.isNaN(AvgRoot)) {
      System.out.println();// breakpoint
    }
    return AvgRoot;// Range should be 0.0 to 2.0, (if 2 is node box size).
  }
  public static int ExplodeCnt = 0;
  public String GetDate() {
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();
    return dtf.format(now);
  }
  public void Explode() {// do if node is stubbornly nonlinear
    int NumPnts = this.Motes.size();
    double RandAmp = 1.0;
    RandAmp = 1.0 / this.Num_Us;
//    RandAmp *= 0.1;
    Mote pnt;
    this.planeform.Randomize(-RandAmp, +RandAmp);
    for (int pcnt = 0; pcnt < NumPnts; pcnt++) {
      pnt = Motes.get(pcnt);
      pnt.Randomize(-RandAmp, +RandAmp);
    }
    this.FitErrorFlywheel = 0.0;
    System.out.println("Explode! " + GetDate() + ", " + ExplodeCnt);
    ExplodeCnt++;
  }
  public NodeBox Clone_Me() {
    NodeBox child = new NodeBox();
    return child;
  }
}
