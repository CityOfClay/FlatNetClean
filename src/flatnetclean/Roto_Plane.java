package flatnetclean;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;

/**
 *
 * @author MultiTool
 */
/* ****************************************************** */
public class Roto_Plane extends PlaneNd implements Things.Drawable {
  // the purpose of this class is to represent a sigmoid plane, to fit it to points, and to fit points to it.
  private PointNd PingVec;
  PointNd normal = null;
  PointNd desire = null;
  // These values below should come from NodeBox context!
  int xorg, yorg;
  /* ****************************************************** */
  public Roto_Plane(int num_dims) {
    super(num_dims);
    PingVec = new PointNd(ndims);
    normal = new PointNd(ndims);
    desire = new PointNd(ndims);
    double RandAmp = 1.0 / this.ninputs;
//    this.Fill(1.0 / this.ninputs, this.ninputs);
//    this.Jitter(-0.001, 0.001);
//    this.Randomize(-0.1, 0.1);
    this.Randomize(-RandAmp, RandAmp);
//    this.Randomize(-0.5, 0.5);
  }
  /* ****************************************************** */
  public Roto_Plane(Roto_Plane donor) {
    super(donor.ndims);
    PingVec = new PointNd(ndims);
    normal = new PointNd(ndims);
    desire = new PointNd(ndims);
    this.Copy_From(donor);
  }
  /* ****************************************************** */
  public void Get_Attraction_To_Plane(PointNd pnt, PointNd Attraction) {// attract mote to flat plane
    double nadir_hgt = this.Get_Plane_Height(pnt);// height on raw plane at this point's position.
    double delta_hgt = nadir_hgt - pnt.loc[ninputs];// distance from this point toward flat plane.
    PointNd normal = new PointNd(ndims);
    this.Plane_Ramp_To_Normal(normal);// get normal to raw plane.
    Attraction.Clear();// use Attraction as temporary vector to project delta onto normal
    Attraction.loc[ninputs] = delta_hgt;
    double corrlen = Attraction.Dot_Product(normal);// project delta onto normal, to get straight distance to plane.
    normal.Multiply(corrlen);// multiply unit normal by corrector length to get correction vector.
    Attraction.Copy_From(normal);
  }
  /* ****************************************************** */
  public void Ping(PointNd mote) {// attract plane to mote
    if (mote == null) {
      boolean nop = true;// breakpoint
    }
    double MoteHgt = mote.Get_Height();
    // First get the height of the plane at this loc.
    double PlaneHgt, CurveHgt;
    PlaneHgt = this.Get_Plane_Height(mote);// get height of plane at location of this point
    CurveHgt = Globals.General_ActFun(PlaneHgt);// map to curve
    // Corrector is the distance from the curved plane TOWARD this point's height
    double corr = MoteHgt - CurveHgt;
    if (false) {
      corr *= Globals.sigmoid_deriv(MoteHgt);// junk
    }
    // Now create the recognition vector, based on this pnt's position, and this plane's height offset dimension
    PingVec.Copy_From(mote);
    PingVec.Set_Height(PlaneHgt + corr);
    this.Attract_Plane_To_Mote(PingVec, Things.lrate);
    if (false) {// made things worse, as expected
      this.Limiter();
    }
  }
  /* ****************************************************** */
  public void Limiter() {// experiment
    double Sum = 0.0;
    double Slope;
    for (int cnt = 0; cnt < this.ninputs; cnt++) {
      Slope = this.loc[cnt];
      Sum += Math.abs(Slope);
    }
    if (Sum > 1.0) {
      for (int cnt = 0; cnt < this.ninputs; cnt++) {
        Slope = this.loc[cnt];
        this.loc[cnt] = Slope / Sum;
      }
    }
  }
  /* ****************************************************** */
  public double GetSquishedPlaneHeight(PointNd pnt) {
    double PlaneHeight = this.Get_Plane_Height(pnt);
    return Globals.General_ActFun(PlaneHeight);
  }
  /* ****************************************************** */
  public void Find_Closest_On_Curve(Mote mote) {
    double Fineness = 0.01;
    double WalkerNadirSquishedHeight, PrevDistance, Distance, MoteDeltaHeight;
    PointNd Increment = new PointNd(this), Walker = new PointNd(mote);

    double FirstSign = 1.0;
    if (true) {// snox kludgey. start from normal of plane and walk backwards.
      this.Get_Attraction_To_Plane(mote, Walker);// attraction to FLAT plane
      Walker.Add(mote);// add mote to Walker (closest) to get closest point on flat plane to mote
      FirstSign = -1.0;
    }

    Increment.Set_Height(0.0);
    Increment.Unitize();
    Increment.Multiply(Fineness * FirstSign);
    WalkerNadirSquishedHeight = this.GetSquishedPlaneHeight(Walker);
    MoteDeltaHeight = mote.Get_Height() - WalkerNadirSquishedHeight;// if mote is above its nadir, then +, if below, then -. 
    Increment.Multiply(MoteDeltaHeight > 0.0 ? 1.0 : -1.0);
    Distance = Math.abs(MoteDeltaHeight);// first distance is straight down
    for (int cnt = 0; cnt < 4; cnt++) {
      do {// while getting closer
        PrevDistance = Distance;
        Walker.Add(Increment);
        WalkerNadirSquishedHeight = this.GetSquishedPlaneHeight(Walker);
        Walker.Set_Height(WalkerNadirSquishedHeight);
        Distance = mote.Get_Distance(Walker);
//        System.out.println(Walker.To_String() + " " + Distance);
      } while (PrevDistance >= Distance);
      Increment.Multiply(-Fineness);// reverse direction and increase resolution
    }
    // at this point, PrevDistance will be the closest.  Prev Walker? 
    Walker.Subtract(mote);// correction vector is Walker.Subtract(mote);
    mote.Attraction.Copy_From(Walker);
//    System.out.println();
  }
  /* ****************************************************** */
  public void Attract_Plane_To_Mote(PointNd Invec, double lrate) {
    // Caveat: this only trains for raw plane. must de-sigmoid Invec first to work right. 
    int Ninputs_Local = this.ninputs;//.ndims;
    double Plane_Height = this.Get_Plane_Height(Invec);
    double Goal_Height = Invec.Get_Height();// final dimension is height of mote 
    double Corrector = Goal_Height - Plane_Height;
    double adj, Sum_Squares, Sum_Squares_Inverse;
    Sum_Squares = Invec.Magnitude_Squared(Ninputs_Local);
    Sum_Squares += 1.0;// Invec dewclaw is always 1.0.
    Sum_Squares_Inverse = 1.0 / Sum_Squares;// not used yet
    for (int cnt = 0; cnt < Ninputs_Local; cnt++) {
      adj = (Invec.loc[cnt] / Sum_Squares);// unitary adjustment 
      adj = adj * Corrector * lrate;
      this.loc[cnt] += adj;
    }
    adj = (1.0 / Sum_Squares);// unitary adjustment 
    adj = adj * Corrector * lrate;
    this.loc[Ninputs_Local] += adj;
  }
  /* ****************************************************** */
  @Override public void Draw_Me(Things.DrawingContext dc) {
    Graphics2D gr = dc.gr;
    Things.TransForm mytrans = new Things.TransForm();
    mytrans.Accumulate(dc.transform, this.xorg, this.yorg, 1.0, 1.0);
    Plot_Gradient(dc);
  }
  public void Plot_Gradient(Things.DrawingContext dc) {
    /* all about the gradient for display */
    Things.TransForm tr = dc.transform;
    Graphics2D g2 = dc.gr;
    this.Plane_Ramp_To_Normal(normal);
    double hgt = this.loc[0];
    double grad_x0;
    double grad_y0;
    double grad_x1, grad_y1;
    PointNd steepest = new PointNd(ndims);
    normal.Get_Steepest(steepest);
    double[] ratios = new double[ninputs];// the ratios are NOT the inverse slopes.  they are from the steepest line.
    for (int cnt = 0; cnt < ninputs; cnt++) {
      ratios[cnt] = steepest.loc[cnt] / steepest.loc[ninputs];// inverse slope for each shadow of the steepest
    }
    if (true) {
      double offset = this.loc[ninputs];
      double height0 = -1.0 - offset;
      double height1 = 1.0 - offset;
      double brad = tr.xscale;// Bounds.Rad(ninputs);
      //gradx0 = (int) (brad * (height0 * ratios[0])); grad_y0 = (int) (brad * (height0 * ratios[1])); grad_x1 = (int) (brad * (height1 * ratios[0])); grad_y1 = (int) (brad * (height1 * ratios[1]));
      height0 *= brad;
      height1 *= brad;
      grad_x0 = (int) ((height0 * ratios[0]));
      grad_y0 = (int) ((height0 * ratios[1]));
      grad_x1 = (int) ((height1 * ratios[0]));
      grad_y1 = (int) ((height1 * ratios[1]));
    }
    Color StartColor = new Color(0.0f, 0.0f, 1.0f);//Color startColor = Color.blue;
    Color EndColor = new Color(1.0f, 0.0f, 0.0f);//Color endColor = Color.red;

    GradientPaint gradient;
//        gradient = new GradientPaint((float) (tr.xoffs + grad_x0), (float) (tr.yoffs + grad_y0), StartColor, (float) (tr.xoffs + grad_x1), (float) (tr.yoffs + grad_y1), EndColor);// A non-cyclic gradient
    Point2D.Double StartPnt, EndPnt;
    StartPnt = new Point2D.Double((tr.xoffs + grad_x0), (tr.yoffs + grad_y0));
    EndPnt = new Point2D.Double((tr.xoffs + grad_x1), (tr.yoffs + grad_y1));

    if (StartPnt.distance(EndPnt) == 0.0) {
      g2.setPaint(Color.green);
    } else {
      gradient = new GradientPaint(StartPnt, StartColor, EndPnt, EndColor);
      g2.setPaint(gradient);
    }

    if (true) {
      int rxorg = (int) (tr.xoffs - tr.xscale);
      int ryorg = (int) (tr.yoffs - tr.yscale);
      int rwdt = (int) (2.0 * tr.xscale);
      int rhgt = (int) (2.0 * tr.yscale);
//      System.out.println("rxorg:" + rxorg + ", ryorg:" + ryorg + ", rwdt:" + rwdt + ", rhgt:" + rhgt + "");

      g2.fillRect(rxorg, ryorg, rwdt, rhgt);
      g2.setColor(Color.white);

      double lx0 = (tr.xoffs + grad_x0), ly0 = (tr.yoffs + grad_y0), lx1 = (tr.xoffs + grad_x1), ly1 = (tr.yoffs + grad_y1);

      // x0:-36358.0, y0:29433.0, wdt:42555.0, hgt:-33854.0
      // lx0:-31412.0, ly0:-17270.0, lx1:34434.0, ly1:18996.0
//      System.out.println("lx0:" + lx0 + ", ly0:" + ly0 + ", lx1:" + lx1 + ", ly1:" + ly1 + "");
      double limit = 2000;
      if (Math.abs(lx0) > limit) {
        return;
      }
      if (Math.abs(ly0) > limit) {
        return;
      }
      if (Math.abs(ly0) > limit) {
        return;
      }
      if (Math.abs(ly1) > limit) {
        return;
      }
      g2.drawLine((int) lx0, (int) ly0, (int) lx1, (int) ly1);
    }
  }
}
