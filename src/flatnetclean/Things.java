package flatnetclean;

import java.util.ArrayList;
import java.awt.*;
import java.awt.geom.Point2D;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author MultiTool
 */
public class Things {
  public static int ndims_init = 1;// 3 dimensions.  First dimension is a gimme, becuase it is the output/fire value.
  public static double BoxSpacing = 40.0;
  public static int ColumHeight = 16;
  public static double lrate = 0.1;//0.2;//0.5;//0.9;//0.01;//
  /* ****************************************************** */
  public interface Drawable {
    public void Draw_Me(DrawingContext dc);
  }
  /* ****************************************************** */
  public interface Causal {
    void Collect_And_Fire();
    void Distribute_Outfire();
    void Pass_Back_Correctors();
    void Gather_And_Apply_Correctors();
  }
  /* ****************************************************** */
  public static class TransForm {
    public double xoffs = 0.0, yoffs = 0.0;
    public double xscale = 1.0, yscale = 1.0;
    public void To_Screen(double xloc, double yloc, PointNd answer) {
      answer.loc[0] = xoffs + (xloc * xscale);
      answer.loc[1] = yoffs + (yloc * yscale);
    }
    public void Accumulate(TransForm parent, double xoffsp, double yoffsp, double xscalep, double yscalep) {
      // create my local transform by adding local context to parent context
      // wrong code, just a place holder
      PointNd org = new PointNd(2);
      parent.To_Screen(xoffsp, yoffsp, org);
      this.xoffs = org.loc[0];
      this.yoffs = org.loc[1];
      this.xscale = parent.xscale * xscalep;
      this.yscale = parent.yscale * yscalep;
    }
  }
  /* ****************************************************** */
  public static class DrawingContext {// Let's be final until we can't anymore
    public Graphics2D gr;
    public TransForm transform;
    public int RecurseDepth;
    /* ********************************************************************************* */
    public DrawingContext() {
      this.RecurseDepth = 0;
      this.transform = new TransForm();
    }
    /* ********************************************************************************* */
    public DrawingContext(DrawingContext Fresh_Parent, TransForm Fresh_Transform) {
      this.gr = Fresh_Parent.gr;
      this.RecurseDepth = Fresh_Parent.RecurseDepth + 1;
    }
    /* ********************************************************************************* */
    public Point2D.Double To_Screen(double XLoc, double YLoc) {
      Point2D.Double pnt = null;
//    pnt = new Point2D.Double(this.GlobalOffset.UnMapTime(XLoc), this.GlobalOffset.UnMapPitch(YLoc));
      return pnt;
    }
    /* ********************************************************************************* */
    public void Compound(Things.TransForm other) {
    }
  }
  /* ****************************************************** */
}
/*
 Junkyard

 How to fill an array with exactly N objects randomly distributed, efficiently with no overlaps
 int coincnt = 2;
 int size = 4;
 int numleft = size;
 array[size];

 for (array){
 double odds = coincnt/numleft;
 if (random(1.0)<odds){
 array[cnt] = true;
 coincnt--;
 if (coincnt<=0){break;}
 }
 numleft--;
 }

 */
